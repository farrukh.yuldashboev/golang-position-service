package service

import (
	"context"
	"fmt"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/pkg/helper"
	"bitbucket.org/Udevs/position_service/pkg/logger"
	"bitbucket.org/Udevs/position_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type attributeService struct {
	logger  logger.Logger
	storage storage.StorageI
}

func NewAttributeService(db *sqlx.DB, log logger.Logger) *attributeService {
	return &attributeService{
		logger:  log,
		storage: storage.NewStoragePg(db),
	}
}

func (s *attributeService) Create(ctx context.Context, req *position_service.Attribute) (*position_service.AttributeId, error) {
	id, err := s.storage.Attribute().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while create profession", req, codes.Internal)
	}

	return &position_service.AttributeId{
		Id: id,
	}, nil
}

func (s *attributeService) GetAll(ctx context.Context, req *position_service.GetAllAttributeRequest) (*position_service.GetAllAttributeResponse, error) {
	resp, err := s.storage.Attribute().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all professions", req, codes.Internal)
	}
	fmt.Printf("%+v", resp)
	return resp, nil
}

func (s *attributeService) Get(ctx context.Context, req *position_service.AttributeId) (*position_service.Attribute, error) {
	profession, err := s.storage.Attribute().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting  profession", req, codes.Internal)
	}

	return profession, nil
}

func (s *attributeService) Update(ctx context.Context, req *position_service.Attribute) (*position_service.StatusAttribute, error) {
	status, err := s.storage.Attribute().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while updating  profession", req, codes.Internal)
	}

	return &position_service.StatusAttribute{
		Status: status,
	}, nil
}

func (s *attributeService) Delete(ctx context.Context, req *position_service.AttributeId) (*position_service.StatusAttribute, error) {
	status, err := s.storage.Attribute().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting  profession", req, codes.Internal)
	}

	return &position_service.StatusAttribute{
		Status: status,
	}, nil
}
