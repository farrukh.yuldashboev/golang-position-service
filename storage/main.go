package storage

import (
	"bitbucket.org/Udevs/position_service/storage/postgres"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Profession() repo.ProfessionRepoI
	Attribute() repo.AttributeRepoI
	Position() repo.PositionRepoI
}

type storagePg struct {
	db         *sqlx.DB
	profession repo.ProfessionRepoI
	attribute  repo.AttributeRepoI
	position   repo.PositionRepoI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:         db,
		profession: postgres.NewProfessionRepo(db),
		attribute:  postgres.NewAttributeRepo(db),
		position:   postgres.NewPositionRepo(db),
	}
}

func (s *storagePg) Profession() repo.ProfessionRepoI {
	return s.profession
}

func (s *storagePg) Attribute() repo.AttributeRepoI {
	return s.attribute
}

func (s *storagePg) Position() repo.PositionRepoI {
	return s.position
}
