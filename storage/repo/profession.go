package repo

import (
	"bitbucket.org/Udevs/position_service/genproto/position_service"
)

type ProfessionRepoI interface {
	Create(req *position_service.Profession) (string, error)
	GetAll(req *position_service.GetAllProfessionRequest) (*position_service.GetAllProfessionResponse, error)
	Get(id string) (*position_service.Profession, error)
	Update(req *position_service.Profession) (string, error)
	Delete(id string) (string, error)
}
