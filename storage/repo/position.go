package repo

import (
	"bitbucket.org/Udevs/position_service/genproto/position_service"
)

type PositionRepoI interface {
	Create(req *position_service.CreatePositionRequest) (string, error)
	GetAll(req *position_service.GetAllPositionRequest) (*position_service.GetAllPositionResponse, error)
	Get(id string) (*position_service.GetPositionResponse, error)
	Update(req *position_service.GetPositionResponse) (string, error)
	Delete(id string) (string, error)
}
