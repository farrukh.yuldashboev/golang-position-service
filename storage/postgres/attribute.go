package postgres

import (
	"database/sql"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type attributeRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewAttributeRepo(db *sqlx.DB) repo.AttributeRepoI {
	return &attributeRepo{db: db}
}

func (r *attributeRepo) Create(req *position_service.Attribute) (string, error) {
	var (
		err error
		tx  *sql.Tx
		id  uuid.UUID
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := `INSERT INTO attribute (
				"id",
				"name",
				"type"
			) 
			VALUES ($1, $2, $3) `

	_, err = tx.Exec(query, id, req.Name, req.AttributeType)

	if err != nil {
		return "", err
	}

	return id.String(), nil
}

func (r *attributeRepo) GetAll(req *position_service.GetAllAttributeRequest) (*position_service.GetAllAttributeResponse, error) {
	var (
		filter     string
		args       = make(map[string]interface{})
		count      int32
		attributes []*position_service.Attribute
	)

	if req.Name != "" {
		filter += " AND name ilike '%' || :name || '%' "
		args["name"] = req.Name
	}

	countQuery := `SELECT count(1) FROM attribute WHERE true ` + filter
	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return nil, err
		}
	}

	query := `SELECT
					"id",
					"name",
					"type"
				FROM attribute WHERE true ` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var attribute position_service.Attribute

		err = rows.Scan(
			&attribute.Id,
			&attribute.Name,
			&attribute.AttributeType,
		)

		if err != nil {
			return nil, err
		}

		attributes = append(attributes, &attribute)
	}

	return &position_service.GetAllAttributeResponse{
		Attributes: attributes,
		Count:      count,
	}, nil

}

func (r *attributeRepo) Get(id string) (*position_service.Attribute, error) {
	var attribute position_service.Attribute

	query := `SELECT "id", "name", "type" FROM attribute WHERE id = $1`

	row := r.db.QueryRow(query, id)
	err := row.Scan(
		&attribute.Id,
		&attribute.Name,
		&attribute.AttributeType,
	)

	if err != nil {
		return nil, err
	}

	return &attribute, nil
}

func (r *attributeRepo) Update(req *position_service.Attribute) (string, error) {
	var (
		err error
		tx  *sql.Tx
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	query := `
		UPDATE attribute
		SET name = $1, type=$2		
		WHERE id = $3
	`

	_, err = tx.Exec(query, req.Name, req.AttributeType, req.Id)

	if err != nil {
		return "", err
	}

	return "updated", nil
}

func (r *attributeRepo) Delete(id string) (string, error) {
	query := `DELETE FROM attribute WHERE id = $1`

	_, err := r.db.Exec(query, id)

	if err != nil {
		return "", err
	}

	return "deleted", nil
}
