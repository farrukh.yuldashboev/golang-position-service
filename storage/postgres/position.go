package postgres

import (
	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"database/sql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type positionRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewPositionRepo(db *sqlx.DB) repo.PositionRepoI {
	return &positionRepo{db: db}
}

func (r *positionRepo) Create(req *position_service.CreatePositionRequest) (string, error) {
	var (
		err          error
		tx           *sql.Tx
		position_id  uuid.UUID
		attribute_id uuid.UUID
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	position_id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := `INSERT INTO position (
				id,
				name,
				profession_id,
				company_id
			) 
			VALUES ($1, $2, $3, $4) `

	_, err = tx.Exec(query, position_id, req.Name, req.ProfessionId, req.CompanyId)

	if err != nil {
		return "", err
	}

	query = `INSERT INTO position_attributes (
		id,
		attribute_id,
		position_id,
		value
	) 
	VALUES ($1, $2, $3, $4) `

	positionAttributes := req.PositionAttributes

	for key := range positionAttributes {
		attribute_id, err = uuid.NewRandom()
		if err != nil {
			return "", err
		}
		_, err = tx.Exec(query, attribute_id, positionAttributes[key].AttributeId, position_id, positionAttributes[key].Value)

		if err != nil {
			return "", err
		}
	}

	return position_id.String(), nil
}

func (r *positionRepo) GetAll(req *position_service.GetAllPositionRequest) (*position_service.GetAllPositionResponse, error) {
	var (
		filter    string
		args      = make(map[string]interface{})
		count     int32
		positions []*position_service.GetPositionResponse
	)

	if req.Name != "" {
		filter += " AND position.name ilike '%' || :name || '%' "
		args["name"] = req.Name
	}

	if req.ProfessionId != "" {
		filter += " AND position.profession_id = :profession_id"
		args["profession_id"] = req.ProfessionId
	}

	if req.CompanyId != "" {
		filter += " AND position.company_id = :company_id"
		args["company_id"] = req.CompanyId
	}

	countQuery := `SELECT count(1) FROM position WHERE true ` + filter
	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return nil, err
		}
	}

	query := `SELECT
				position.id,
				position.name,
				position.profession_id,
				position.company_id,
				position_attributes.id,
				position_attributes.attribute_id,
				position_attributes.value,
				attribute.id,
				attribute.name,
				attribute.type
			FROM
				position
			LEFT JOIN
				position_attributes
			ON
				position.id = position_attributes.position_id
			LEFT JOIN
				attribute
			ON
				position_attributes.attribute_id = attribute.id
			WHERE true` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			position          position_service.GetPositionResponse
			positionAttribute position_service.GetPositionAttribute
			attribute         position_service.Attribute
		)
		err = rows.Scan(
			&position.Id,
			&position.Name,
			&position.ProfessionId,
			&position.CompanyId,
			&positionAttribute.Id,
			&positionAttribute.AttributeId,
			&positionAttribute.Value,
			&attribute.Id,
			&attribute.Name,
			&attribute.AttributeType,
		)

		if err != nil {
			return nil, err
		}

		position.PositionAttributes = append(position.PositionAttributes, &position_service.GetPositionAttribute{
			Id:          positionAttribute.Id,
			AttributeId: positionAttribute.AttributeId,
			Value:       positionAttribute.Value,
			Attribute:   &attribute,
		})
		positions = append(positions, &position)
	}

	return &position_service.GetAllPositionResponse{
		Positions: positions,
		Count:     count,
	}, nil

}

func (r *positionRepo) Get(id string) (*position_service.GetPositionResponse, error) {
	var position position_service.GetPositionResponse
	// var PositionAttributes position_service.GetPositionAttribute

	queryPosition := `
				SELECT 
					id,
					name,
					profession_id,
					company_id
				FROM
					position
				WHERE
					id = $1`
	row := r.db.QueryRow(queryPosition, id)
	err := row.Scan(
		&position.Id,
		&position.Name,
		&position.ProfessionId,
		&position.CompanyId,
	)

	if err != nil {
		return nil, err
	}

	queryPositionAttributes := `
				SELECT
					pa.id,
					pa.attribute_id,
					pa.value,
					a.id,
					a.name,
					a.type
				FROM
					position_attributes pa
				INNER JOIN
					attribute a
				ON
					pa.attribute_id =	a.id
	
				WHERE 
					pa.position_id = $1`

	rows, _ := r.db.Query(queryPositionAttributes, position.Id)
	for rows.Next() {
		var PositionAttributes position_service.GetPositionAttribute
		var attribute position_service.Attribute
		err = rows.Scan(
			&PositionAttributes.Id,
			&PositionAttributes.AttributeId,
			&PositionAttributes.Value,
			&attribute.Id,
			&attribute.Name,
			&attribute.AttributeType,
		)

		if err != nil {
			return nil, err
		}

		position.PositionAttributes = append(position.PositionAttributes, &position_service.GetPositionAttribute{
			Id:          PositionAttributes.Id,
			AttributeId: PositionAttributes.AttributeId,
			Value:       PositionAttributes.Value,
			Attribute:   &attribute,
		})
	}

	return &position, nil
}

func (r *positionRepo) Update(req *position_service.GetPositionResponse) (string, error) {
	var (
		err error
		tx  *sql.Tx
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	query := `
		UPDATE
			position
		SET
			name = $1,
			profession_id = $2,
			company_id = $3
		WHERE
			id = $4
	`

	_, err = tx.Exec(query, req.Name, req.ProfessionId, req.CompanyId, req.Id)

	if err != nil {
		return "", err
	}

	query = `
		UPDATE
			position_attributes
		SET
			attribute_id = $1,
			value = $2
		WHERE
			position_id = $3
	`
	p_attributes := req.PositionAttributes
	for key := range p_attributes {
		_, err = tx.Exec(query, p_attributes[key].AttributeId, p_attributes[key].Value, req.Id)

		if err != nil {
			return "", err
		}
	}

	// query = `
	// 	UPDATE
	// 		attribute
	// 	SET
	// 		name = $1,
	// 		type = $2
	// 	WHERE
	// 		id = $3
	// `
	// for key := range p_attributes {
	// 	_, err = tx.Exec(query, p_attributes[key].Attribute.Name, p_attributes[key].Attribute.AttributeType, p_attributes[key].AttributeId)

	// 	if err != nil {
	// 		return "", err
	// 	}
	// }
	return "updated", nil
}

func (r *positionRepo) Delete(id string) (string, error) {
	var (
		err   error
		tx    *sql.Tx
		pa_id string
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "error in DELETE transaction", err
	}
	query := `SELECT 
					id
				FROM
					position_attributes
				WHERE
					position_id=$1`
	row := r.db.DB.QueryRow(query, id)
	err = row.Scan(
		&pa_id,
	)
	if err != nil {
		return "error: could not GET position_attribute ID", err
	}

	query = `DELETE FROM position_attributes WHERE ID=$1`
	_, err = r.db.Exec(query, pa_id)

	if err != nil {
		return "error: could not DELETE position_attributeS table", err
	}

	query = `DELETE FROM position WHERE ID=$1`
	_, err = r.db.Exec(query, id)

	if err != nil {
		return "error: could not DELETE position table", err
	}

	return "deleted", nil
}
