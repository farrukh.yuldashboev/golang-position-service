CREATE TABLE IF NOT EXISTS "profession" (
    "id" uuid primary key,
    "name" varchar(255) not null
);

CREATE TYPE IF NOT EXISTS "attribute_types" AS ENUM (
  'datetime',
  'text',
  'number'
);

CREATE TABLE IF NOT EXISTS "attribute" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "type" attribute_types
);
