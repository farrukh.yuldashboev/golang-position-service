CREATE TABLE IF NOT EXISTS "position" (
    "id" uuid primary key,
    "name" varchar not null,
    "profession_id" uuid not null references profession(id),
    "company_id" uuid not null
);

CREATE TABLE IF NOT EXISTS "position_attributes" (
    "id" uuid primary key,
    "attribute_id" uuid not null references attribute(id),
    "position_id" uuid not null references position(id),
    "value" varchar not null
);